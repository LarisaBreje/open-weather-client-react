import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/home";
import Forecast from "./pages/forecast";
import Header from "./components/header/header";

function App() {
  return (
      <Router>
        <Header />
        <Routes>
          <Route index element={<Home />} />
          <Route path="/forecast" element={<Forecast/>}/>
        </Routes>
      </Router>
  );
}

export default App;
