import { createContext, useState } from "react";


export const Ctx = createContext();



const Provider = ({ children }) => {
    const [search, setSearch] = useState();

    return(
        <Ctx.Provider value={{search, setSearch}}>{children}</Ctx.Provider>
    )
}

export default Provider;