import { RiCelsiusFill } from 'react-icons/ri'

export const getWeatherParams = (data, paramName) => {
    const obj = {
        feels_like: <div>{`Feels like ${Math.floor(data.feels_like)}`}<RiCelsiusFill className="ms-2"/></div>,
        humidity: <div>{`Humidity is ${data.humidity} %`}</div>,
        pressure: <div>{`Pressure is ${data.pressure} hPa`}</div>,
        temp: <div>{`Temperature is ${data.temp}`}<RiCelsiusFill  className="ms-2"/></div>,
        temp_max: <div>{`Maxim Temperature will be ${data.temp_max}`}<RiCelsiusFill  className="ms-2"/></div>,
        temp_min: <div>{`Minimum Temperature will be ${data.temp_min}`}<RiCelsiusFill  className="ms-2"/></div>,
    }
    return obj[paramName];
}


/*
    const obj = {
        '2023-06-14': {
            data: [
                {
                    h: '00:00',
                    data: {}
                }
            ]
        }
    }

 */
export const formatForecastData = (data) => {
    const obj = {};
    data.forEach(item => {
        const day = item.dt_txt.split(" ")[0]
        const h = item?.dt_txt.split(" ")[1]
        if(!obj[day]){
            obj[day] = [item]
        }
        else {
            obj[day].push(item);
        }
    })
    return obj;
}