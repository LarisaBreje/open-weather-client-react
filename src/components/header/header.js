import { Nav, Navbar, Offcanvas, Container, Form, Button } from "react-bootstrap";
import { useContext } from "react";
import { Ctx } from "../../context/store";
import { Link } from "react-router-dom";

const Header = () => {

    const { setSearch } = useContext(Ctx);

    const expand = "md"

    const handleSearch = (e) => {
        e.preventDefault();
        setSearch(e.target[0].value);
    }

    return(
        <Navbar bg="light" expand={expand} className="mb-3">
        <Container fluid>
          <Navbar.Brand>Open Weather Api Client</Navbar.Brand>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-${expand}`}
            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
            placement="end"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
              Open Weather Api Client
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Link to="/" className="nav-link">Home</Link>
                <Link to="/forecast" className="nav-link">Forecast</Link>
              </Nav>
              <Form className="d-flex" onSubmit={handleSearch}>
                <Form.Control
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                />
                <Button variant="outline-success" type="submit">Search</Button>
              </Form>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    )
}

export default Header;