import axios from "axios";

const api = axios.create({
    baseURL: "https://api.openweathermap.org/data/2.5",
    headers:{
        "Accept": "application/json"
    }
})

export const getCurrentWeatherByCity = (city) => {
    return api.get(`/weather?q=${city}&appid=${process.env.REACT_APP_API_KEY}&units=metric`);
}

export const getForecastByCity = (city) => {
    return api.get(`/forecast?q=${city}&appid=${process.env.REACT_APP_API_KEY}&units=metric`);
}


export const getCurrentWeatherByLocation = (lat, lon) => {
    return api.get(`/weather?lat=${lat}&lon=${lon}&appid=${process.env.REACT_APP_API_KEY}&units=metric`);
}