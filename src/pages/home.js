import { useEffect, useState, useContext } from 'react';
import { getCurrentWeatherByCity, getCurrentWeatherByLocation } from '../services/api';
import { getWeatherParams } from '../utils/weatherApiUtils';
import { Ctx } from '../context/store';

const Home = () => {
    const [weatherData, setWeatherData] = useState();
    const { search, setSearch } = useContext(Ctx);

    useEffect(() => {
        async function fetchData() {
            try{
                const res = await getCurrentWeatherByCity(search);
                if(res.status === 200){
                    setWeatherData(res.data)
                }
            }
            catch(error){
                console.log(error)
            }
        }
        if(search)fetchData();
        else {
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition((position) => {
                    getCurrentWeatherByLocation(position.coords.latitude, position.coords.longitude)
                    .then(res => {
                        setSearch(res.data.name)
                    })
                    .catch(error => console.log(error))
                })
            }
        }
    }, [search])

    return (
        <div className='m-2 text-center'>
            <h1>{weatherData?.name}</h1>
            {weatherData?.weather[0]?.icon && <div>
                <img src={`http://openweathermap.org/img/wn/${weatherData?.weather[0].icon}@2x.png`} alt="weather icon"/>
                <p>{weatherData?.weather[0].description}</p>
            </div>}
            <div>
                {weatherData?.main && Object.keys(weatherData?.main).map((key) => (
                    <div key={key}>{getWeatherParams(weatherData?.main, key)}</div>
                ))}
            </div>
        </div>
    )
}

export default Home;