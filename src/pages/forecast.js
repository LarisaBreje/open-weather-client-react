import { useState, useContext, useEffect } from 'react';
import { Ctx } from '../context/store';
import { getForecastByCity } from '../services/api';
import { Table } from 'react-bootstrap';
import { formatForecastData } from "../utils/weatherApiUtils";
import { RiCelsiusFill } from 'react-icons/ri'

const Forecast = () => {
    const [forecastData, setForecastData] = useState();
    const { search } = useContext(Ctx);

    useEffect(() => {
        async function fetchData() {
            try {
                const res = await getForecastByCity(search);
                if (res.status === 200) {
                    const dormatedData = formatForecastData(res.data.list)
                    setForecastData(dormatedData)
                }
            }
            catch (error) {
                console.log(error)
            }

        }
        if (search) fetchData()
    }, [search])

    const hours = ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00']

    return (
        <div className="m-2">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Day</th>
                        {hours.map(h => <th key={h}>{h}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {forecastData && Object.keys(forecastData).map(day => (
                        <tr key={day + "--"}>
                            <td>{day}</td>
                            {hours.map(h => {
                                const foundItem = forecastData[day].find(item => item.dt_txt.split(" ")[1] === h + ":00")
                                if (foundItem) {
                                    return <td key={h + "=="}>{foundItem.main.temp}<RiCelsiusFill /></td>
                                }
                                else return <td key={h + ">>"}></td>
                            })}
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    )
}

export default Forecast;